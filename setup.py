"""Project setup."""
from setuptools import setup

# Set app dependencies here
setup(
    name='veridian',
    packages=['app'],
    version='1.0',
    install_requires=[
        'Flask-Migrate',
        'Flask-CORS',
        'Flask-Session',
        'Flask-Ext',
        'Flask',
        'click',
        'crayons',
        'gunicorn',
        'sqlalchemy-searchable',
        'sqlalchemy-utils',
        'SQLAlchemy',
        'redis',
        'psycopg2',
        'PyMySQL',
        'jwcrypto',
        'bcrypt',
        'python-dotenv',
        'cryptography == 3.0'
    ]
)
