"""App entry point."""
from app.flask import flask_app
from app.migrate import migrate
from app.db import db
import app.routes
import app.handlers
import app.cli

# Run application
if __name__ == 'main':
    flask_app.run()
