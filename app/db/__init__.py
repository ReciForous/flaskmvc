from flask import _app_ctx_stack
from app.flask import flask_app
from sqlalchemy import (
    create_engine,
    MetaData
)
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base

engine = create_engine(
    flask_app.config['SQLALCHEMY_DATABASE_URI']
    # connect_args={'check_same_thread': False}
)
session = sessionmaker(autocommit=False, autoflush=False, bind=engine)


class Alchemy(object):
    """Custom DB class object."""

    def __init__(self, meta=MetaData()):
        """
        Instantiate Alchemy class instance.

        @params: self
        @return: None
        """
        self.Session = session
        self.metadata = meta
        self.Model = declarative_base(metadata=self.metadata)
        self.engine = engine

    def __repr__(self):
        """
        Represent stringified Alchemy class instance method.

        @params: self
        @return: string
        """
        return f'<Alchemy(Session={self.Session}, metadata={self.metadata}, Model={self.Model})>'

    def setMetaData(self, meta=None):
        self.metadata = meta

    def setModel(self, model):
        self.Model = model

    def setSession(self, session=session()):
        self.Session = session


db = Alchemy()

# Register a scoped session for flask to use
flask_app.db_session = scoped_session(
    db.Session,
    scopefunc=_app_ctx_stack.__ident_func__
)
