"""Permissions cli."""
import click
from flask.cli import AppGroup
from app.models.Permission import Permission
from app.db import db
from app.flask import flask_app
from sqlalchemy.exc import SQLAlchemyError

permission_cli = AppGroup(
    'permission',
    short_help='Permissions cli'
)


@permission_cli.command('sync')
@click.option('--remove-dangling/--keep-dangling', default=False)
def sync_permissions(remove_dangling):
    """Create app permissions based on registered app routes."""
    session = db.Session()
    permissions = session.query(Permission).all()
    new_permissions = []
    endpoints = [rule.endpoint for rule in flask_app.url_map.iter_rules()]
    for endpoint in endpoints:
        if not any(
            p.slug == endpoint for p in permissions
        ):
            name = endpoint.replace('.', ' ').title()
            name = name.replace('_', ' ')
            new_permissions.append(
                Permission(name=name, slug=endpoint)
            )
    if remove_dangling:
        for permission in permissions:
            if permission.slug not in endpoints:
                session.delete(permission)
    try:
        session.bulk_save_objects(new_permissions)
        session.commit()
        click.echo('App permission have been updated.')
    except SQLAlchemyError as e:
        session.rollback()
        click.echo(e)
    finally:
        session.close()
