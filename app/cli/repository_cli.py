"""Repository cli to generate repository files."""
import os
import stat
import click
from flask.cli import AppGroup
from app.flask import flask_app

repository_cli = AppGroup(
    'repository',
    short_help="Flask repository cli."
)


@repository_cli.command('create')
@click.argument('name')
def create_repository(name):
    """Create repository file."""
    pass
