"""Controller cli to generate controller files."""
import os
import stat
import click
from flask.cli import AppGroup
from app.flask import flask_app

controller_cli = AppGroup(
    'controller',
    short_help="Flask controller cli."
)


@controller_cli.command('create')
@click.argument('name')
@click.option('-a', '--api')
def create_controller(name, api=False):
    """Create flask web or a api controller."""
    if not api:
        file_path = f'app/controllers/{name}.py'
        file_imports = (
            'import click\n' +
            'from flask import (\n' +
            '    Blueprint,\n' +
            '    request,\n' +
            '    render_template\n' +
            ')\n'
        )
    else:
        file_path = f'app/controllers/api/{name}.py'
        file_imports = (
            'import click\n' +
            'from flask import (\n' +
            '    Blueprint,\n' +
            '    request,\n' +
            '    jsonify\n' +
            ')\n' +
            '# Import repo here\n'
        )

    file_body = (
        f'{"_" + name if api else name} = Blueprint(\n' +
        f"    '{name}',\n" +
        '    __name__\n' +
        ')\n\n' +
        f'repo = None\n\n' +
        f"@{name}.route('/', methods=['GET'])\n" +
        'def index():\n' +
        '    return repo.index()\n\n' +
        f"@{name}.route('/', methods=['POST'])\n" +
        'def store():\n' +
        '    return repo.store(request.get_json())\n\n' +
        f"@{name}.route('/<int:id>', methods=['GET'])\n" +
        'def show(id):\n' +
        '    return repo.show(id)\n\n' +
        f"@{name}.route('/<int:id>', methods=['PATCH'])\n" +
        'def update(id):\n' +
        '    return repo.update(id, request.get_json())\n\n' +
        f"@{name}.route('/<int:id>', methods=['DELETE'])\n" +
        'def destroy(id):\n' +
        '    return repo.destroy(id)\n\n'
    )

    file_contents = file_imports + file_body

    f = open(file_path, 'w+')
    f.write(file_contents)
    f.close()
    os.chmod(file_path, stat.S_IROTH | stat.S_IWOTH)
