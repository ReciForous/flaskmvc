"""Register custom flask cli commands."""
from app.flask import flask_app
from app.cli.model_cli import model_cli
from app.cli.controller_cli import controller_cli
from app.cli.key_cli import key_cli
from app.cli.permission_cli import permission_cli

flask_app.cli.add_command(model_cli)
flask_app.cli.add_command(controller_cli)
flask_app.cli.add_command(key_cli)
flask_app.cli.add_command(permission_cli)
