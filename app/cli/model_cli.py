import os
import stat
import click
from flask.cli import AppGroup
from app.flask import flask_app

model_cli = AppGroup(
    'model',
    short_help="Flask model cli."
)


@model_cli.command('create')
@click.argument('name')
@click.option('-t', '--tablename')
def create_model(name, tablename=None):
    """Create a model file."""
    if tablename is None:
        tablename = f"'{name.lower()}s'"
    else:
        tablename = tablename.split('=')
        if len(tablename) > 1:
            tablename = f"'{tablename[1]}'"
        else:
            tablename = f"'{tablename[0]}'"

    file_imports = (
        'from app.models.BaseModel import BaseModel\n' +
        'from app.db import db\n\n\n'
    )
    file_body = (
        f'class {name.capitalize()}(db.Model, BaseModel):\n' +
        f"   __name__ = '{name.capitalize()}'\n" +
        f"   __tablename__ = {tablename}\n\n" +
        '    id = db.Column(db.Integer, primary_key=True)\n\n' +
        '    validation_rules = []\n\n' +
        '    attributes = []\n\n' +
        '    guarded = []\n\n' +
        '    relations = []\n\n' +
        '    sortable = {}\n\n'
    )
    file_contents = file_imports + file_body
    file_path = f'app/models/{name.capitalize()}.py'
    file = open(file_path, 'w+')
    file.write(file_contents)
    file.close()
    os.chmod(file_path, stat.S_IROTH | stat.S_IWOTH)
