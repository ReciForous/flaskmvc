import os
import stat
import click
import string
import random
import bcrypt
import base64
from hashlib import sha256
from flask.cli import AppGroup
from app.flask import flask_app

key_cli = AppGroup(
    'key',
    short_help="Generate keys for .env file"
)


@key_cli.command('generate')
@click.option('-l', '--length', type=int)
def generate(length=None):
    """Generate a secure app key."""
    env_file_path = '.env'
    env_file_edited = False

    characters = string.ascii_letters + string.digits
    phrase_length = 32

    if length:
        phrase_length = length

    phrase = ''.join((random.choice(characters) for i in range(phrase_length)))
    click.echo('Phrase used: ' + phrase)

    key = bcrypt.hashpw(
        # Base64 encode to avoid null byte errors in bcrypt
        base64.b64encode(sha256(phrase.encode()).digest()),
        bcrypt.gensalt()
    )
    key = key.decode()
    click.echo('Key generated: ' + key)

    env_file = open(env_file_path, 'r')
    file_contents = env_file.readlines()
    env_file.close()

    for content in file_contents:
        line_no = file_contents.index(content)
        env_variable = content.split('=')[0]
        if env_variable == 'FLASK_APP_KEY':
            file_contents[line_no] = f'FLASK_APP_KEY={key}\n'
            env_file_edited = True

    file_mode = 'w' if env_file_edited else 'a'
    env_file = open(env_file_path, file_mode)

    if env_file_edited:
        env_file.writelines(file_contents)
    else:
        env_file.write(f'\nFLASK_APP_KEY={key}\n')

    env_file.close()
