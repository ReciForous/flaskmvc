from os import environ
from dotenv import dotenv_values
import click


class Config(object):
    env_file_path = '.env'

    def __init__(self, env_file_path=None):
        self.configs = {}
        if env_file_path:
            self.env_file_path = env_file_path
        self.configs.update(dotenv_values(self.env_file_path))
        self.configs.update(environ)


configs = Config().configs
