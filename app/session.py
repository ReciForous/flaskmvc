from os import environ
from flask_session import Session
from app.flask import flask_app

SESSION_TYPE = environ.get('FLASK_SESSION_TYPE', '')
Session(flask_app)
