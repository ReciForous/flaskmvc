"""Register custom error handles here."""
from app.errors.InvalidRequest import InvalidRequest
from app.flask import flask_app
from flask import jsonify


@flask_app.errorhandler(InvalidRequest)
def handle_invalid_request(error):
    """Handle Invalid handle request."""
    return jsonify(error.to_dict()), error.status_code
