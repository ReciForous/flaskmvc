from app.repositories.BaseAPIRepository import BaseAPIRepository
from app.models.User import User
from flask import (
    jsonify
)
from sqlalchemy.exc import SQLAlchemyError
from app.flask import flask_app

class UserRepository(BaseAPIRepository):
    model = User
    allowed_includes = ['role', 'role.permissions']
    def store(self, data):
        user = self.model(**data)
        user.hashpw()
        try:
            flask_app.db_session.add(user)
            flask_app.db_session.commit()
            user = user.serialize()
            status = 201
            message = f'{self.model.__name__} created successfully.'
        except SQLAlchemyError as e:
            flask_app.db_session.rollback()
            user = None
            status = 500
            message = f'Error: Could not create {self.model.__name__}.'
        finally:
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': user}), status

    def activate(self, id):
        user = flask_app.db_session.query(self.model).get(id)

        if user.is_active:
            return jsonify({
                'message': 'User is already active',
                'data': user.serialize()
            }), 200

        user.is_active = True
        try:
            flask_app.db_session.commit()
            status = 200
            message = f'{self.model.__name__} activated successfully.'
        except SQLAlchemyError as e:
            flask_app.db_session.rollback()
            status = 500
            message = f'Error: Could not activate {self.model.__name__}.'
        finally:
            user = user.serialize()
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': user})

    def disable(self, id):
        user = flask_app.db_session.query(self.model).get(id)

        if not user.is_active:
            return jsonify({
                'message': 'User is already disabled',
                'data': user.serialize()
            }), 200

        user.is_active = False
        try:
            flask_app.db_session.commit()
            status = 200
            message = f'{self.model.__name__} disabled successfully.'
        except SQLAlchemyError as e:
            flask_app.db.session.rollback()
            status = 500
            message = f'Error: Could not disable {self.model.__name__}.'
        finally:
            user = user.serialize()
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': user})
