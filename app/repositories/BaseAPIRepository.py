import click
from flask import (
    Blueprint,
    request,
    jsonify,
    url_for
)
from app.flask import flask_app
from app.helper import (
    get_includes,
    get_filters,
    get_sort_bys,
    get_page,
    get_page_size,
    paginate
)
from app.errors.InvalidRequest import InvalidRequest
from sqlalchemy.exc import SQLAlchemyError
from app.validators.user_validator import UserValidator
from app.middlewares.request_validator import validate_request
import click


class BaseAPIRepository():
    model = None
    allowed_includes = []
    allowed_filters = []

    def index(self):
        includes = get_includes()
        for include in includes:
            if include not in self.allowed_includes:
                raise InvalidRequest(f'Include not allowed: {include}', 400)

        filters = get_filters()
        sort_by = get_sort_bys()

        query = flask_app.db_session.query(self.model)
        if sort_by:
            keys = list(sort_by.keys())
            key = keys[0]
            sort_by = sort_by[key]
            if (key in self.model.sortable) and sort_by == 'asc':
                try:
                    query = query.order_by(self.model.sortable[key].asc())
                except Exception:
                    raise InvalidRequest(
                        f'{key} does not allow sorting as is.',
                        400
                    )
            elif (key in self.model.sortable) and sort_by == 'desc':
                try:
                    query = query.order_by(self.model.sortable[key].desc())
                except Exception:
                    raise InvalidRequest(
                        f'{key} does not allow sorting as is.',
                        400
                    )
            else:
                if key in self.model.relationships:
                    query = query.join(getattr(self.model, key))
                else:
                    raise InvalidRequest(
                        f'Sort by {key} is an invalid relationship or not allowed.',
                        400
                    )
                sortable = self.model.sortable[key]
                while isinstance(sort_by, dict):
                    key = list(sort_by.keys())[0]
                    sort_by = sort_by[key]
                    try:
                        sortable = sortable[key]
                    except KeyError:
                        raise InvalidRequest(
                            f'Relationship does not allow filtering by {key}',
                            400
                        )
                if sort_by == 'asc':
                    query = query.order_by(sortable.asc())
                if sort_by == 'desc':
                    query = query.order_by(sortable.desc())

        if filters:
            query = query.filter_by(**filters)

        query = query.all()
        paginated = paginate(
            query,
            get_page(),
            get_page_size()
        )
        paginated['data'] = [
            item.serialize(
                with_relations=includes
            ) for item in paginated['data']
        ]
        flask_app.db_session.close()
        return jsonify(paginated), 200

    def show(self, id):
        includes = get_includes()
        for include in includes:
            if include not in self.allowed_includes:
                raise InvalidRequest(f'Include not allowed: {include}', 400)
        item = flask_app.db_session.query(self.model).get(id)
        if not item:
            raise InvalidRequest('Error: Requested resource not found', 404)
        item = item.serialize(with_relations=includes)
        flask_app.db_session.close()
        return jsonify({'data': item}), 200

    def store(self, data):
        item = self.model(**data)
        try:
            flask_app.db_session.add(item)
            flask_app.db_session.commit()
            item = item.serialize()
            status = 201
            message = f'{self.model.__name__} created successfully.'
        except SQLAlchemyError as e:
            flask_app.db_session.rollback()
            item = None
            status = 500
            message = f'Error: Could not create {self.model.__name__}.'
        finally:
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': item}), status

    def update(self, id, data):
        item = flask_app.db_session.query(self.model).get(id)
        if not item:
            raise InvalidRequest('Error: Requested resource not found', 404)

        for attribute in self.model.attributes:
            setattr(item, attribute, data[attribute])

        try:
            flask_app.db_session.commit()
            item = item.serialize()
            status = 200
            message = f'{self.model.__name__} updated successfully.'
        except SQLAlchemyError as e:
            flask_app.db_session.rollback()
            item = None
            status = 500
            message = f'Error: Could not update {self.model.__name__}'
        finally:
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': item}), status

    def destroy(self, id):
        item = flask_app.db_session.query(self.model).get(id)
        if not item:
            raise InvalidRequest('Error: Requested resource not found', 404)

        try:
            flask_app.db_session.delete(item)
            flask_app.db_session.commit()
            status = 200
            item = None
            message = f'{self.model.__name__} deleted successfully'
        except SQLAlchemyError as e:
            flas_app.db_session.rollback()
            item = None
            status = 500
            message = f'Error: Could not delete {self.model.__name__}'
        finally:
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': item}), status
