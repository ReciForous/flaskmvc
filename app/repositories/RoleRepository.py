"""Role repository."""
from app.repositories.BaseAPIRepository import BaseAPIRepository
from app.models.Role import Role
from app.models.Permission import Permission
from app.errors.InvalidRequest import InvalidRequest
from flask import (
    jsonify
)
from sqlalchemy.exc import SQLAlchemyError
from app.flask import flask_app
from app.helper import (
    paginate,
    get_page,
    get_page_size
)


class RoleRepository(BaseAPIRepository):
    """Role repository."""

    model = Role
    allowed_includes = ['permissions']

    def store_permissions(self, id, data):
        """Store permissions to a role."""
        role = flask_app.db_session.query(self.model).get(id)
        for permission in data['permissions']:
            permission = flask_app.db_session.query(Permission).get(
                permission['id']
            )
            if(permission):
                role.permissions.append(permission)
        try:
            flask_app.db_session.commit()
            message = 'Permissions added to role successfully.'
            status = 201
        except SQLAlchemyError:
            flask_app.db_session.rollback()
            message = 'Could not add permissions to role.'
            status = 500
        finally:
            role = role.serialize(with_relations=['permissions'])
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': role}), status

    def index_permissions(self, id):
        """Get role permissions."""
        role = flask_app.db_session.query(self.model).get(id)
        permissions = role.permissions
        paginated = paginate(
            permissions,
            get_page(),
            get_page_size()
        )
        paginated['data'] = [
            item.serialize() for item in paginated['data']
        ]
        return jsonify(paginated), 200

    def destroy_permission(self, id, permission_id):
        """Delete role permission."""
        role = flask_app.db_session.query(self.model).get(id)

        try:
            permission = [
                p for p in role.permissions if p.id == permission_id
            ][0]
        except IndexError:
            raise InvalidRequest(
                'Role does not have the requested permission',
                404
            )

        role.permissions.remove(permission)
        try:
            flask_app.db_session.commit()
            message = 'Permission removed from role successfully'
            status = 200
        except SQLAlchemyError:
            flask_app.db_session.rollback()
            message = 'Could not remove permission from role.'
            status = 500
        finally:
            role = role.serialize(with_relations=['permissions'])
            flask_app.db_session.close()
            return jsonify({'message': message, 'data': role}), status
