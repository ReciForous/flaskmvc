"""User class."""
from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    DateTime,
)
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey
from app.db import db
import bcrypt
import base64
from datetime import datetime
from hashlib import sha256
from app.models.BaseModel import BaseModel


class User(db.Model, BaseModel):
    """
    SQLAlchemy User model.

    Inherits: BaseModel
    """
    __name__ = 'User'
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String(120), nullable=False)
    email = Column(String(120), nullable=False, unique=True)
    password = Column(String(120), nullable=False)
    phone = Column(Integer, nullable=True, unique=True)
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, onupdate=datetime.utcnow)
    role_id = Column(Integer, ForeignKey('roles.id'))
    role = relationship('Role', uselist=False, lazy="joined")

    # Validation rules for model
    validation_rules = [
        {'attr': 'name', 'required': True},
        {'attr': 'email', 'required': True, 'unique': True, 'type': 'email'},
        {'attr': 'phone', 'required': True, 'unique': True, 'type': 'int'},
        {'attr': 'password', 'required': True, 'type': 'password'},
        {'attr': 'role_id', 'required': True, 'type': 'int'}
    ]

    # Guarded attributes to prevent being passed to frontend
    guarded = ['password']

    # Mass assignable atrributes
    attributes = [
        'name',
        'email',
        'phone',
        'is_active',
        'role_id'
    ]

    # Model relationships
    relationships = [
        'role'
    ]

    # Map sortable attributes to columns
    sortable = {
        'id': id,
        'created_at': created_at,
        'updated_at': updated_at,
        'role': role_id,
        'name': name
    }

    def hashpw(self):
        """
        Hash password using bcrypt.

        args: self
        return-type: None.
        """
        password = self.password
        password = base64.b64encode(sha256(password.encode()).digest())
        self.password = bcrypt.hashpw(password, bcrypt.gensalt())
