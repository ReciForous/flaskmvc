from app.models.BaseModel import BaseModel
from app.db import db


class Profile(db.Model, BaseModel):
   __name__ = 'Profile'
   __tablename__ = 'profiles'

    id = db.Column(db.Integer, primary_key=True)

    validation_rules = []

    attributes = []

    guarded = []

    relations = []

    sortable = {}

