"""BaseModel.py."""
from app.helper import hyphenated
import click


class BaseModel():
    """Base model class."""

    validation_rules = []

    guarded = []

    attributes = []

    relationships = []

    filterable = []

    sortable = {}

    def serialize(self, with_relations=[]):
        """
        Serializer func to properly convert models to a dict with relations.

        @args: self
        @kwargs: with_relations

        @returns: dict
        """
        if not isinstance(with_relations, list):
            raise TypeError('Relations is not a list')

        serialized = {'type': hyphenated(self.__name__)}

        if self.id:
            serialized['id'] = self.id

        for attribute in self.attributes:
            if attribute not in self.guarded:
                serialized[attribute] = getattr(self, attribute, None)

        for relationship in with_relations:
            relations = relationship.split('.', 1)
            relation_name = relations[0]
            nested_relations = []
            if len(relations) > 1:
                nested_relations = relations[1].split('.')

            if (relation_name in self.relationships) and (relation_name not in self.guarded):
                relation = getattr(self, relation_name, None)
                if isinstance(relation, list):
                    relation = [
                        related.serialize(
                            with_relations=nested_relations
                        ) for related in relation
                    ]
                elif relation:
                    relation = relation.serialize(
                        with_relations=nested_relations
                    )
                if relation_name in serialized:
                    serialized[relation_name].update(relation)
                else:
                    serialized.update(
                        {relation_name: relation}
                    )

        return serialized
