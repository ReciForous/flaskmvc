from sqlalchemy import (
    Column,
    Integer,
    String,
    Boolean,
    DateTime,
)
# from sqlalchemy.orm import relationship
# from sqlalchemy.schema import ForeignKey
from app.db import db
from app.models.BaseModel import BaseModel
from datetime import datetime

class Permission(db.Model, BaseModel):
    __name__ = 'Permission'
    __tablename__ = 'permissions'

    id = Column(Integer, primary_key=True)
    name = Column(String(120))
    slug = Column(String(150))
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, onupdate=datetime.utcnow)

    attributes = [
        'slug',
        'name'
    ]
