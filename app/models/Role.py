from app.db import db
from sqlalchemy import (
    Table,
    Column,
    Integer,
    String,
    DateTime,
)
from sqlalchemy.orm import relationship
from sqlalchemy.schema import ForeignKey
from app.models.BaseModel import BaseModel
from app.models.Permission import Permission
from datetime import datetime


# Helper table to map role to permission
role_permissions_table = Table(
    'role_permissions',
    db.Model.metadata,
    Column(
        'role_id',
        Integer,
        ForeignKey('roles.id'),
        nullable=False
    ),
    Column(
        'permission_id',
        Integer,
        ForeignKey('permissions.id'),
        nullable=False
    ),
    Column(
        'created_at',
        DateTime,
        default=datetime.utcnow
    ),
    Column(
        'updated_at',
        DateTime,
        onupdate=datetime.utcnow
    )
)


class Role(db.Model, BaseModel):
    __tablename__ = 'roles'
    __name__ = 'Role'

    id = Column(Integer, primary_key=True)
    name = Column(String(120))
    description = Column(String(120))
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, onupdate=datetime.utcnow)
    permissions = relationship(
        'Permission',
        secondary=role_permissions_table
    )

    attributes = ['name', 'description', 'created_at', 'updated_at']

    relationships = ['permissions']

    validation_rules = [
        {'attr': 'name', 'required': True},
        {'attr': 'description', 'required': True}
    ]

    sortable = {
        'id': id,
        'name': name,
        'created_at': created_at,
        'updated_at': updated_at,
        'permissions': {
            'slug': Permission.slug
        }
    }
