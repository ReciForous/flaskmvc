from flask import (
    Blueprint,
    request,
    render_template
)

home = Blueprint(
    'home',
    __name__
)

@home.route('/', defaults={'path': ''})
@home.route('/<path:path>')
def index(path):
    return render_template('index.html', title='Hermes')


@home.route('/login', methods=['GET', 'POST'])
def login():
    return render_template('login.html')

@home.route('/register', methods=['GET'])
def register_get():
    return render_template('register.html')

@home.route('/register', methods=['POST'])
def register_post():
    return render_template('register.html')
