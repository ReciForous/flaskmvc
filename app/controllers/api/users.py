import click
from flask import (
    Blueprint,
    request,
    jsonify
)
from app.repositories.UserRepository import UserRepository
from app.validators.user_validator import UserValidator
from app.middlewares import validate_request

_users = Blueprint(
    'users',
    __name__
)
repo = UserRepository()


# Restful api implementation
@_users.route('/', methods=['POST'], strict_slashes=False)
@validate_request(custom_validator=UserValidator)
def store():
    return repo.store(request.get_json())


@_users.route('/', methods=['GET'], strict_slashes=False)
def index():
    return repo.index()


@_users.route('/<int:id>', methods=['GET'])
def show(id):
    return repo.show(id)


@_users.route('/<int:id>', methods=['PATCH'])
def update(id):
    return repo.update(id, request.get_json())


@_users.route('/<int:id>/activate', methods=['PATCH'])
def activate(id):
    return repo.activate(id)


@_users.route('/<int:id>/disable', methods=['PATCH'])
def disable(id):
    return repo.disable(id)
