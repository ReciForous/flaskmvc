"""Authentication endpoints."""
import bcrypt
import base64
from hashlib import sha256
from flask import (
    request,
    jsonify,
    Blueprint
)
from jwcrypto import (
    jwk,
    jws,
    jwe,
    jwt
)
from app.models.User import User
from app.middlewares import validate_request
from app.flask import flask_app
from app.helper import k
from app.errors.InvalidRequest import InvalidRequest

_auth = Blueprint(
    'auth',
    __name__
)


@_auth.route('/', methods=['POST'], strict_slashes=False)
@validate_request(
    [
        {'attr': 'email', 'type': 'email', 'required': True},
        {'attr': 'password', 'type': 'password', 'required': True}
    ]
)
def auth():
    """Authenticate user."""
    if request.headers['content-type'] == 'application/json':
        user_json = request.get_json()
        user = flask_app.db_session.query(User).filter_by(
            email=user_json['email']
        ).first()

        if not user:
            return jsonify({
                'authenticated': False,
                'token': None
            })

        password = user_json['password']
        password = base64.b64encode(sha256(password.encode()).digest())

        if bcrypt.checkpw(password, user.password.encode()):
            # Generate JWT token and return with token
            claims = {
                'iss': f'{flask_app.config["CLIENT_ID"]} - {flask_app.config["CLIENT_NAME"]}',
                'aud': f'{request.environ["REMOTE_ADDR"]}',
                'user': user.serialize()
            }

            header = {
                'alg': 'HS512'
            }

            key = jwk.JWK(**k)

            token = jwt.JWT(header=header, claims=claims)
            token.make_signed_token(key)
            token = token.serialize()

            return jsonify({
                'authenticated': True,
                'token': token
            })

        return jsonify({
            'authenticated': False,
            'token': None
        })
