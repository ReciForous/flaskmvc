import click
from flask import (
    Blueprint,
    request,
    jsonify
)
from app.repositories.RoleRepository import RoleRepository
from app.validators.role_validator import RoleValidator
from app.middlewares import (validate_request, authorized)


_roles = Blueprint(
    'roles',
    __name__
)
repo = RoleRepository()


@_roles.route('/', methods=['POST'])
@validate_request(custom_validator=RoleValidator)
@authorized('roles.store')
def store():
    return repo.store(request.get_json())


@_roles.route('/', methods=['GET'], strict_slashes=False)
@authorized('roles.index')
def index():
    return repo.index()


@_roles.route('/<int:id>', methods=['GET'])
@authorized('roles.show')
def show(id):
    return repo.show(id)


@_roles.route('/<int:id>', methods=['PATCH'])
@authorized('roles.update')
def update(id):
    return repo.update(id, request.get_json())


@_roles.route('/<int:id>', methods=['DELETE'])
@authorized('roles.destroy')
def destroy(id):
    return repo.destroy(id)


@_roles.route('/<int:id>/permissions', methods=['POST'], strict_slashes=False)
@validate_request(validations=[
    {'attr': 'permissions', 'type': 'list', 'keys': ['id']}
])
@authorized('roles.store_permissions')
def store_permissions(id):
    return repo.store_permissions(id, request.get_json())


@_roles.route('/<int:id>/permissions', methods=['GET'], strict_slashes=False)
@authorized('roles.index_permissions')
def index_permissions(id):
    return repo.index_permissions(id)


@_roles.route('/<int:id>/permissions/<int:permission_id>', methods=['DELETE'])
@authorized('roles.destroy_permission')
def destroy_permission(id, permission_id):
    return repo.destroy_permission(id, permission_id)
