"""Authentication middleware."""
import click
import json
from functools import wraps
from flask import request
from jwcrypto import (jwk, jws, jwt)
from app.errors.InvalidRequest import InvalidRequest
from app.helper import (k, get_auth_token)
from app.flask import flask_app


def authenticated(f):
    """
    Authenticate jwtoken.

    @args: None
    @returns: decorated - Decorator function
    @raise: app.errors.InvalidRequest
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        # Properly raises invalid request if auth token is not present.
        auth_token = get_auth_token()

        key = jwk.JWK(**k)

        jwtoken = jwt.JWT(key=key, jwt=auth_token)

        sig = jws.JWS()
        sig.deserialize(auth_token)

        try:
            sig.verify(key)
        except jws.InvalidJWSSignature:
            raise InvalidRequest(
                message='Invalid authentication token',
                status_code=403
            )

        claims = json.loads(jwtoken.claims)

        if 'iss' not in claims:
            raise InvalidRequest(
                message='Invalid authentication token',
                status_code=403
            )

        if not claims['iss'].strip(' - ')[0] == flask_app.config['CLIENT_ID']:
            raise InvalidRequest(
                message='Invalid authentication token',
                status_code=403
            )

        if 'aud' not in claims:
            raise InvalidRequest(
                message='Invalid authentication token',
                status_code=403
            )

        if not claims['aud'] == str(request.environ['REMOTE_ADDR']):
            raise InvalidRequest(
                message='Invalid authentication token',
                status_code=403
            )

        if 'user' not in claims:
            raise InvalidRequest(
                message='Invalid authentication token',
                status_code=403
            )
        return f(*args, **kwargs)
    return decorated
