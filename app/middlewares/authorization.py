"""Authorization middleware."""
import json
from functools import wraps
from jwcrypto import (jwk, jwt)
from app.flask import flask_app
from app.models.Role import Role
from app.helper import (k, get_auth_token)
from app.errors.InvalidRequest import InvalidRequest
import click


def authorized(slug):
    """
    Check if user is authorized to the allowed resource.

    @args: slug - Permission slug to check for
    @returns: decorator function
    @raise: app.errors.InvalidRequest
    """
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            auth_token = get_auth_token()
            key = jwk.JWK(**k)

            jwtoken = jwt.JWT(key=key, jwt=auth_token)
            claims = json.loads(jwtoken.claims)
            user = claims['user']
            role = flask_app.db_session.query(Role).get(user['role_id'])
            permissions = role.permissions
            if not any(
                permission.slug == slug for permission in permissions
            ):
                raise InvalidRequest(
                    message='You are not authorized to access this resource.',
                    status_code=403
                )
            return f(*args, **kwargs)
        return wrapper
    return decorator
