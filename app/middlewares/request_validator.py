from app.validators.validator import Validator
from functools import wraps
from app.errors.InvalidRequest import InvalidRequest
from flask import request

def validate_request(validations=None, custom_validator=None):
    """
    Decorated method for request validation.

    args: validations: dict, custom_validator: Validator
    """
    if custom_validator is not None:
        validator = custom_validator()
    else:
        validator = Validator()

    # Create decorator function
    def decorator(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            json = request.get_json()
            valid, message = validator.validated(json, validations)
            if not valid:
                raise InvalidRequest(message=message)
            return f(*args, **kwargs)
        return wrapper
    return decorator
