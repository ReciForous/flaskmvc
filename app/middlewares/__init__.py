"""Register middlewares."""
from app.middlewares.authentication import authenticated
from app.middlewares.authorization import authorized
from app.middlewares.request_validator import validate_request
