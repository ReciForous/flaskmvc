from app.flask import flask_app
from app.controllers.web.home import home
from app.controllers.api.users import _users as api_users
from app.controllers.api.auth import _auth as api_auth
from app.controllers.api.roles import _roles as api_roles

# Web Routes
flask_app.register_blueprint(home, url_prefix='/')

# Api routes
flask_app.register_blueprint(api_users, url_prefix='/api/v1/users')
flask_app.register_blueprint(api_roles, url_prefix='/api/v1/roles')
flask_app.register_blueprint(api_auth, url_prefix='/api/v1/auth')
