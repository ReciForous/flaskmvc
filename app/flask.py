"""Register flask application."""
from app.configs import configs
from flask import (
    Flask
)
from flask_cors import CORS
import click

# Flask configurations
flask_app = Flask(
    __name__,
    template_folder='views'
)
flask_app.config['SQLALCHEMY_DATABASE_URI'] = configs['FLASK_DATABASE_URI']

flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = configs[
    'FLASK_SQLALCHEMY_TRACK_MODIFICATIONS'
]

flask_app.config['PUBLIC_KEY'] = configs['FLASK_PUBLIC_KEY']

flask_app.config['CLIENT_ID'] = configs['FLASK_CLIENT_ID']

flask_app.config['CLIENT_NAME'] = configs['FLASK_CLIENT_NAME']

flask_app.config['JSON_SORT_KEYS'] = False


# Register cross origin resource sharing routes
cors = CORS(flask_app, resource={r'/api/*': {'origins': '*'}})


@flask_app.teardown_appcontext
def remove_session(*args, **kwargs):
    """Remove db session."""
    flask_app.db_session.remove()
