"""Register alembic migrations for flask."""
from flask_migrate import Migrate
from app.flask import flask_app
from app.db import db

# Import project models here
from app.models.User import User
from app.models.Permission import Permission
from app.models.Role import (Role, role_permissions_table)

migrate = Migrate(flask_app, db)
