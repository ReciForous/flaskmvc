"""Register helper functions."""
from flask import (
    request
)
import click
import re
from app.flask import flask_app

hyphenation_pattern = re.compile(r'(?<!^)(?=[A-Z])')
k = {'k': flask_app.config['PUBLIC_KEY'], 'kty': 'oct'}


def get_includes():
    """Helper method to get relations when fetching records."""
    includes = []
    if 'includes' in request.args:
        includes = request.args['includes'].split(',')
    return includes


def get_filters():
    """Helper method to get filters when fetching records."""
    filters = {}
    filters = parse_dict_params_to_dict(request.args, 'filters')

    return filters


def get_sort_bys():
    """Helper method to get sort bys when fetching records."""
    sort_bys = {}
    sort_bys = parse_dict_params_to_dict(request.args, 'sort_by')

    return sort_bys


def parse_dict_params_to_dict(params, param_name):
    """Parse a given param name to a dict since flask by default doesnt."""
    parsed = {}
    pattern = re.compile(fr'{param_name}*')
    key_pattern = re.compile(r'(\[(.*?)\])')
    keys = params.keys()
    for key in keys:
        if pattern.match(key):
            # Strip param name from params
            attr_keys = key.split(param_name)[1]
            # Reverse list to build a nested dict
            # Eg. sort_by[permissions][slug] - Needed for relationships
            attr_keys = list(reversed(
                [k[1] for k in key_pattern.findall(attr_keys)]
            ))
            # Check if list has more than 1 element
            if len(attr_keys) > 1:
                # Build dict
                parsed = build_tree_dict(attr_keys, initial=params[key])
            else:
                # If attr keys not greater than one assign straight up
                parsed[attr_keys[0]] = params[key]
    return parsed


def get_page():
    """Helper method to get page for indexing routes."""
    page = 1
    if 'page' in request.args:
        page = int(request.args['page'])

    return page


def get_page_size():
    """Helper method to get page size for indexing routes."""
    page_size = 15
    if 'page_size' in request.args:
        page_size = int(request.args['page_size'])

    return page_size


def paginate(source, page, page_size):
    """Helper method to create pagination dictionary for indexing routes."""
    records_total = len(source)
    if records_total < 1:
        return {
            'data': [],
            'records_total': records_total,
            'pages': 0,
            'current_page': page,
            'prev_page': None,
            'next_page': None,
            'page_size': page_size
        }
    items = list(split_results(source, page_size))
    return {
        'data': items[page - 1],
        'records_total': records_total,
        'pages': len(items),
        'current_page': page,
        'prev_page': page - 1 if not (page - 1) < 1 else None,
        'next_page': page + 1 if not (page + 1) > len(items) - 1 else None,
        'page_size': page_size
    }


def split_results(source, size):
    """Generator method to create a uniform split list."""
    for i in range(0, len(source), size):
        yield source[i:i + size]


def hyphenated(source):
    """Hyphenate a given camel cased string."""
    return hyphenation_pattern.sub('-', source).lower()


def build_tree_dict(keys, initial={}):
    """Build nested dict based on a list of keys."""
    tree_dict = {}
    for key in keys:
        # If index of key is 0 assign final value of last key
        # Else nest dict in dict
        if keys.index(key) == 0:
            tree_dict = {key: initial}
        else:
            tree_dict = {key: tree_dict}
    return tree_dict


def nested_dict_has_keys(val, keys):
    """Check if nested dict has keys."""
    if not isinstance(val, dict):
        raise AttributeError(f'Dictionary expected got {type(val)}')
    if not isinstance(keys, list):
        raise AttributeError('Keys expected to be a list')
    el = val
    for key in keys:
        try:
            el = el[key]
        except KeyError as e:
            return False
    return True


def get_auth_token():
    """
    Helper function to get auth token from headers.

    @raise: InvalidRequest
    """
    if 'Authorization' not in request.headers:
        raise InvalidRequest(
            message={
                'content': 'Authorization header missing in request',
                'status': False
            },
            status_code=403
        )

    try:
        auth_token = request.headers['Authorization'].split(' ')[1]
    except IndexError:
        raise InvalidRequest(
            message={
                'content': 'Token missing in authorization header',
                'status': False
            },
            status_code=403
        )

    return auth_token