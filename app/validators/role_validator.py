from app.validators.validator import Validator
from app.models.Role import Role


class RoleValidator(Validator):
    model = Role
