from app.validators.validator import Validator
from app.models.User import User


class UserValidator(Validator):
    model = User
