import re
import click
from app.flask import flask_app

class Validator:
    model = None

    def __init__(
        self,
        email_regex=r"""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""",
        password_regex=r"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])"
    ):
        """
        Constructor for the validator class.

        args: [self, email_regex: String, password_regex: String]

        returns: None
        """
        self.email_regex = email_regex
        self.password_regex = password_regex

    def validated(self, obj, validations=None,):
        """
        Main validation method.

        args: [self, obj: dict, validations: dict]

        validations keys: [
            required: boolean,
            length: int | dict,
            type: string,
            attr: String
        ]

        if length is dict keys: [
            min: int,
            max: int
        ]

        returns: Tuple (Boolean, String)
        """
        if validations is None:
            validations = self.model.validation_rules

        for validation in validations:
            if 'attr' not in validation:
                raise AttributeError('Error: Validation dictionary is missing attribute key')

            attr = validation['attr']

            if 'required' in validation and validation['required']:
                if not (attr in obj) or not obj[attr]:
                    return False, f'Error: {attr} is required'

            if 'unique' in validation and validation['unique']:
                valid, message = self.is_unique(obj, attr)
                if not valid:
                    return valid, message

            if 'type' in validation:
                valid, message = self.type_validation(validation, obj, attr)
                if not valid:
                    return valid, message

            if 'length' in validation:
                valid, message = self.length_validation(validation, obj, attr)
                if not valid:
                    return valid, message

            if 'keys' in validation:
                valid, message = self.has_keys(obj[attr], validation['keys'])
                if not valid:
                    return valid, message

        return True, 'validation success'

    def type_validation(self, validation, obj, attr):
        """
        Stub function to validate attribute is of type.

        args: [self, validation: dict, obj: dict, attr: string]

        supported types: [email, password, int, float]

        returns: Tuple (Boolean, String)
        """
        attr_type = validation['type']
        if attr_type == 'email':
            try:
                self.validate_email(obj[attr])
            except AssertionError as e:
                return False, f'Error: {attr} is not a valid email'

        elif attr_type == 'password':
            try:
                self.validate_password(obj[attr])
            except AssertionError as e:
                return False, f'Error: {attr} is not valid'

        elif attr_type == 'int':
            if not self.valid_int(obj[attr]):
                return False, f'Error: {attr} is not a valid int'

        elif attr_type == 'float':
            if not self.valid_float(obj[attr]):
                return False, f'Error: {attr} is not a valid float'

        elif attr_type == 'list':
            if not self.valid_list(obj[attr]):
                return False, f'Error: {attr} is not a valid list'

        return True, ''

    def length_validation(self, validation, obj, attr):
        """
        Stub function to validate attribute is of length.

        args: [self, validation: dict, obj: dict, attr: string]

        raises: Attribute error if length is dict and does not have min and
        max keys.

        returns: Tuple (Boolean, String)
        """
        length = validation['length']
        if isinstance(length, dict) and self.valid_length_dict(length):
            try:
                mn = length['min']
                mx = length['max']
                self.validate_length_between(obj[attr], mn, mx)
            except AssertionError as e:
                return False, f'Error: {attr} needs to be between {mn} and {mx} characters'

        elif isinstance(length, int):
            try:
                self.validate_length(obj[attr], length)
            except AssertionError as e:
                return False, f'Error: {attr} needs to be greater than {length} characters'

        else:
            raise AttributeError('Error: length validation is not in a proper format')

        return True, ''

    def validate_email(self, val):
        """
        Assert email matches class instance of email_regex.

        args: [self, val: string]

        raises: Assertion Error

        returns: None
        """
        assert re.match(self.email_regex, val)

    def is_unique(self, obj, attr):
        """
        Check if record with attribute already exists.

        args: [self, obj: dict, attr: string]

        returns Tuple (Boolean, String)
        """
        query = flask_app.db_session.query(self.model).filter_by(
            **{attr: obj[attr]}
        )
        query = query.all()
        if query:
            return False, f'{attr.capitalize()} {obj[attr]} already exists.'

        return True, ''

    def validate_password(self, val):
        """
        Assert password matches class instance of password_regex.

        args: [self, val: string]

        raises: Assertion Error

        returns: None
        """
        assert re.match(self.password_regex, val)

    def valid_int(self, val):
        """
        Check if obj value can be parsed as a proper int.

        args: [val]

        returns: boolean
        """
        try:
            int(val)
            return True
        except ValueError as e:
            return False

    def valid_list(self, val):
        """
        Check if obj value can be parsed as a proper list.

        args: [val]

        returns boolean
        """
        try:
            list(val)
            return True
        except ValueError as e:
            return False

    def has_keys(self, val, keys):
        """
        Check if passed obj has valid keys.

        args: [val]

        returns Tuple (boolean, str)
        """
        if not isinstance(val, list):
            return False, 'object is not an iterateable list'
        for obj in val:
            for key in keys:
                try:
                    obj[key]
                except KeyError as e:
                    return False, f'Missing required key: {key}'
        return True, ''

    def valid_float(self, val):
        """
        Check if obj value can be parsed as a proper float.

        args: [val]

        returns: boolean
        """
        try:
            float(val)
            return True
        except ValueError as e:
            return False

    def validate_length(self, val, length):
        """
        Assert if length of value is greated than defined length.

        args: [val, length]

        raises: AssertionException

        returns: None
        """
        assert len(val) >= length

    def validate_length_between(self, val, mn, mx):
        """
        Assert if length of value is between defined min and max.

        args: [val, mn: int, mx: int]

        raises: AssertionException

        returns: None
        """
        assert len(val) >= mn and len(val) <= mx

    def valid_length_dict(self, length_dict):
        """
        Helper function to check if length dict is valid.

        args: [length_dict: dict]

        returns: boolean
        """
        return 'max' in length_dict and 'min' in length_dict
