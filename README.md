# MVC framework for flask

Simple MVC framework for flask with request validations and SQLAlchemy models

## Setup Instructions

### Docker Setup

Copy paste the following commands to terminal if youre on nix I guess

```

# Make a docker directory behind the current directory with a configs subdirectory
mkdir -p ../docker/configs

# Create a file called app.hermes in docker/configs and echo contents into folder, this is the nginx configurations for the app
touch ../docker/configs/app.hermes && echo "server {
    listen       80;
    server_name  app.hermes;

    location / {
        proxy_pass http://127.0.0.1:8000;
    }
}
" >> ../docker/configs/app.hermes
```

```
# Create a file called app.hermes.conf in docker/configs and echo contents into folder, this is the supervisor configurations for the app 
touch ../docker/configs/app.hermes.conf && echo "[program:hermes]
command=/home/user/flask-app/hermes/bin/gunicorn --reload -b 0.0.0.0:8000 --error-logfile="/var/log/nginx/app.hermes_error.log" --access-logfile="/var/log/nginx/app.hermes_access.log" --forwarded-allow-ips="*" app:flask_app 
directory=/home/user/flask-app
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
" >> ../docker/configs/app.hermes.conf
```

```
# Create a docker-compose file behind the current directory
touch ../docker-compose.yml && echo "version: '3.5'
networks:
        default:
                external: {name: nginx-proxy}
services:
        app:
                container_name: hermes
                environment:
                        - VIRTUAL_HOST=app.hermes
                image: reciforous/python:3.6.12-nginx
                stdin_open: true
                # Run flask with nginx & debug configs
                # command: sh -c "nginx && hermes/bin/flask run"
                links: [db]
                volumes: [
                        './flask-app:/home/user/flask-app',
                        './docker/configs/app.hermes:/etc/nginx/conf.d/app.hermes.conf',
                        './docker/configs/app.hermes.conf:/etc/supervisor.d/app.hermes.conf',
                        # './app/.env.prod:/home/user/flask-app/.env'
                ]
                working_dir: /home/user/flask-app
        db:
                container_name: hermes-db
                environment: {
                        MYSQL_DATABASE: hermes,
                        MYSQL_USER: hermes,
                        MYSQL_PASSWORD: hermes,
                        MYSQL_ROOT_PASSWORD: password
                }
                image: mysql:5.7
                stdin_open: true
                ports: ['3315:3306']
                volumes: ['data:/var/lib/mysql']
volumes:
        data: {}
" >> ../docker-compose.yml
```

### First time start up (Docker)

```
# Start up the docker-compose file
docker-compose up

# Open a new terminal shell and enter into the docker container shell
docker exec -it app sh

# In the docker container start by making a python virtual environment - NOTE: The virtual environment will be persisted and directory called hermes will be created in your project folder
python3 -m venv hermes

# Enter the virtual environment
source hermes/bin/activate

# Install the project dependencies by running setup.py
python3 setup.py develop

# Run migrations in the migrations folder
flask db upgrade

# You can now exit and uncomment the following line in the docker-compose file to run a dev environment with gunicorn and nginx
# command: sh -c "nginx && hermes/bin/flask run"

# To run in a production environment simply just comment the above line back
```

## Project Structure

```
.
├── app             # Root application directory.
│   ├── controllers # Controllers directory.
│   ├── configs.    # Config file to read .env.
│   ├── db          # SQLAlchemy DB configurations.
│   ├── errors      # Custom application error.
│   ├── flask.py    # Flask app registrations.
│   ├── handlers.py # Register custom error handles.
│   ├── helper.py   # Define helper functions.
│   ├── __init__.py # Application entry point.
│   ├── middlewares # Define middlewares for the applicaiton.
│   ├── migrate.py  # Register models for alembic migrations.
│   ├── models      # Models directory.
|   ├── repositories# Repository folder to sit between controllers and models.
│   ├── routes.py   # Application route registrations.
│   ├── static      # Project assets folder.
│   ├── validators  # Define custom validators for requests.
│   └── views       # View folder - Uses flasks default jinja2 templating.
├── .env.example    # Env example.
├── .gitignore
├── migrations      # Migrations folder used by alembic, you normally wont have to mess with this.
│   ├── alembic.ini
│   ├── env.py
│   ├── README
│   ├── script.py.mako
│   └── versions
├── README.md       # You are here. 
├── setup.py        # Project setup file.
└── storage         # Application file storage for uploads and such.
    └── .gitkeep
```